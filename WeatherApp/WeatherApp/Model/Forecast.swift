//
//  Forecast.swift
//  WeatherApp
//
//  Created by Stefan on 2018/08/19.
//  Copyright © 2018 stefan. All rights reserved.
//

import Foundation
class Forecast: Codable {
    var day: String
    var weather_condition: String
    var temp: String
    
    init(day: String, weather_condition:String, temp: String) {
        self.day = day
        self.weather_condition = weather_condition
        self.temp = temp
    }
}
