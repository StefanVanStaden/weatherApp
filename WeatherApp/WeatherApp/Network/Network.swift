//
//  Network.swift
//  WeatherApp
//
//  Created by Stefan on 2018/08/19.
//  Copyright © 2018 stefan. All rights reserved.
//

import Foundation
import Alamofire
typealias ServiceResponse = (Any, Error?) -> Void

class Network:NSObject{
    static let sharedInstance = Network()
    let appID = "e2d7cbd4063f03c57ca699c21ef8132d"
    
    func getCurrent(lat:String!, long:String!, onCompletion: @escaping ServiceResponse) {
        let endpoint = "http://api.openweathermap.org/data/2.5/weather?lat="+lat+"&&lon="+long+"&&APPID=\(appID)"
        Alamofire.request(endpoint)
            .responseJSON { response in
                // check for errors
                guard response.result.error == nil else {
                    // got an error in getting the data, need to handle it
                    print("error calling GET on /todos/1")
                    print(response.result.error!)
                    return
                }
                
                // make sure we got some JSON since that's what we expect
                guard (response.result.value as? [String: Any]) != nil else {
                    print("didn't get todo object as JSON from API")
                    if let error = response.result.error {
                        print("Error: \(error)")
                    }
                    return
                }
                
                onCompletion(response.result.value!,response.result.error)
               
        }
        
    }
    
    func getForecast(lat:String!, long:String!, onCompletion: @escaping ServiceResponse) {
        let endpoint = "http://api.openweathermap.org/data/2.5/forecast/?lat="+lat+"&&lon="+long+"&&APPID=\(appID)"
        Alamofire.request(endpoint)
            .responseJSON { response in
                // check for errors
                guard response.result.error == nil else {
                    // got an error in getting the data, need to handle it
                    print("error calling GET on /todos/1")
                    print(response.result.error!)
                    return
                }
                
                // make sure we got some JSON since that's what we expect
                guard (response.result.value as? [String: Any]) != nil else {
                    print("didn't get todo object as JSON from API")
                    if let error = response.result.error {
                        print("Error: \(error)")
                    }
                    return
                }
                
                onCompletion(response.result.value!,response.result.error)
                
        }
        
    }
    
    
}
