//
//  Current.swift
//  WeatherApp
//
//  Created by Stefan on 2018/08/19.
//  Copyright © 2018 stefan. All rights reserved.
//

import Foundation

class Current: Codable{
    var temp:String
    var temp_min: String
    var temp_max: String
    var name: String
    var weather_condition: String
    
    init(temp:String,temp_min:String,temp_max:String,name: String, weather_condition: String) {
        self.temp = temp
        self.temp_min = temp_min
        self.temp_max = temp_max
        self.name = name
        self.weather_condition = weather_condition
    }
    
    func getCurrentHexColor() -> Int {
        switch weather_condition {
        case "Clear":
            return 0x47AB2F
        case "Rain":
           return 0x57575D
        case "Clouds":
            return 0x54717A
        default: break
        }
        return 0x54717A
    
    }
    
    
}
