//
//  ViewController.swift
//  WeatherApp
//
//  Created by Stefan on 2018/08/19.
//  Copyright © 2018 stefan. All rights reserved.
//

import UIKit
import CoreLocation

class CurrentWeatherController: UIViewController, CLLocationManagerDelegate, UITableViewDelegate, UITableViewDataSource {
    // MARK: - state variables
    var network: Network!
    var current: Current!
    var weekly_forecast: [Forecast]!
    
    // MARK: - Location variables
    var latitude: String?
    var longitude: String?
    var hasUpdated:Bool = false
    let locationManager = CLLocationManager()
    // MARK: - DateFormatter variables
    let dateFormatterGet = DateFormatter()
    let dateFormatterPrint = DateFormatter()
    
    
    // MARK: - UI Elements
    @IBOutlet weak var weatherConditionImageView: UIImageView!
    @IBOutlet weak var currentTemp: UILabel!
    @IBOutlet weak var currentWeatherCondition: UILabel!
    @IBOutlet weak var minTemp: UILabel!
    @IBOutlet weak var currentTempSmall: UILabel!
    @IBOutlet weak var maxTemp: UILabel!
    @IBOutlet weak var temp_background: UIView!
    @IBOutlet weak var forecastTable: UITableView!
    
    // MARK: - ViewController Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
         dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
         dateFormatterPrint.dateFormat = "EEEE"
         weekly_forecast = []
        
        forecastTable.delegate = self
        forecastTable.dataSource = self
        
        let cellNib = UINib(nibName: "ForecastRow", bundle: nil)
        forecastTable.register(cellNib, forCellReuseIdentifier: "ForecastRowCell")
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        network = Network.sharedInstance
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - Location manager
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        latitude = String(locValue.latitude)
        longitude = String(locValue.longitude)
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        locationManager.stopUpdatingLocation()
        if(!hasUpdated){
            updateCurrentWeather()
            hasUpdated = true
        }
        
    }
    
    // MARK: - Weather Updates
    func getCelsius(K:Double) -> String{
        let celsius = K - 273.15
        
        
        return String(format: "%.0f", celsius) + "°"
    }
    func updateCurrentWeather(){
        
            network.getCurrent(lat: latitude, long: longitude) { (json, error) in
                
                print(json)
                let parsedJSON = json as! [String:Any]
                let parsedMain = parsedJSON["main"] as! [String:Any]
                let parsedWeather = parsedJSON["weather"] as! [[String:Any]]
                
                let temp = self.getCelsius(K: parsedMain["temp"] as! Double)
                let temp_max = self.getCelsius(K: parsedMain["temp_max"] as! Double)
                let temp_min = self.getCelsius(K: parsedMain["temp_min"] as! Double)
                let weatherCondition = parsedWeather[0]["main"]
                
                self.current = Current(temp: temp, temp_min: temp_min, temp_max: temp_max, name: parsedJSON["name"] as! String, weather_condition: weatherCondition as! String)
                
            
                self.currentTemp.text = temp
                self.currentTempSmall.text = temp
                self.maxTemp.text = temp_max
                self.minTemp.text = temp_min
                switch weatherCondition as! String{
                case "Clear":
                   self.weatherConditionImageView.image = #imageLiteral(resourceName: "forest_sunny")
                   self.currentWeatherCondition.text = "SUNNY"
                case "Rain":
                    self.weatherConditionImageView.image = #imageLiteral(resourceName: "forest_rainy")
                    self.currentWeatherCondition.text = "RAINY"
                case "Clouds":
                    self.weatherConditionImageView.image = #imageLiteral(resourceName: "forest_cloudy")
                    self.currentWeatherCondition.text = "CLOUDY"
                default: break
                }
                self.temp_background.backgroundColor = UIColor(rgb: self.current.getCurrentHexColor())
                self.forecastTable.backgroundColor = UIColor(rgb: self.current.getCurrentHexColor())
                self.updateForecast()
                
            }
        
//        let defaults = UserDefaults.standard
//        if let email = defaults.string(forKey: "email"){
//            emailButton.text = email
//        }
        
    }
    
    func updateForecast(){
        network.getForecast(lat: latitude, long: longitude) { (json, error) in
            print(json)
            let parsedJSON = json as! [String:Any]
            let list = parsedJSON["list"] as! [[String:Any]]
            var day_count = 0
            for obj in list{
                if(day_count % 8 == 0){
                    let date = self.dateFormatterGet.date(from: obj["dt_txt"] as! String)
                    
                    let parsedWeather = obj["weather"] as! [[String:Any]]
                    let parsedMain = obj["main"] as! [String:Any]
                    let forecast = Forecast(day: self.dateFormatterPrint.string(from: date!), weather_condition: parsedWeather[0]["main"] as! String, temp: self.getCelsius(K: parsedMain["temp_max"] as! Double))
                    self.weekly_forecast.append(forecast)
                }
                day_count += 1
                
                
            }
            self.forecastTable.reloadData()
            
        }
    }
    
    // MARK: - Forecast Tableview
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return weekly_forecast.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let row = indexPath.row
        let forecast = weekly_forecast[row]
        print("Tapped on forecast: " + forecast.day)
    
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ForecastRowCell", for: indexPath as IndexPath) as! ForecastCell
        cell.backgroundColor = UIColor.clear// set default row background
        cell.selectionStyle = .none
        let row = indexPath.row
        let forecast = weekly_forecast[row]
        cell.dayOfWeek.text = forecast.day
        cell.temperature.text = forecast.temp
        switch forecast.weather_condition {
        case "Clear":
            cell.weatherCondition.image = #imageLiteral(resourceName: "clear")
        case "Rain":
             cell.weatherCondition.image = #imageLiteral(resourceName: "rain")
        case "Clouds":
            cell.weatherCondition.image = #imageLiteral(resourceName: "partlysunny")
        default: break
        }
        
        cell.backgroundColor = UIColor(rgb: current.getCurrentHexColor())
        return cell
    }


}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}

