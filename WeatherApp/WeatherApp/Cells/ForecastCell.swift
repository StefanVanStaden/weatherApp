//
//  ForecastCell.swift
//  WeatherApp
//
//  Created by Stefan on 2018/08/19.
//  Copyright © 2018 stefan. All rights reserved.
//

import Foundation
import UIKit

class ForecastCell: UITableViewCell {
    
    @IBOutlet weak var dayOfWeek: UILabel!
    @IBOutlet weak var weatherCondition: UIImageView!
    @IBOutlet weak var temperature: UILabel!
}
